
package fr.campus.academy.backenddev.examen.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("visitor").password(this.passwordEncoder.encode("jesuisunvisiteur"))
                .roles("VISITOR").and()

                .withUser("organizer").password(this.passwordEncoder.encode("jesuisunorganisateur"))
                .roles("ORGANISER", "VISITOR");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().formLogin().and().logout().deleteCookies("JSESSIONID").invalidateHttpSession(true).and()
                .authorizeRequests().antMatchers("/login").permitAll().antMatchers(HttpMethod.PUT).hasRole("ORGANISER")
                .antMatchers(HttpMethod.POST).hasRole("ORGANISER").antMatchers(HttpMethod.DELETE).hasRole("ORGANISER")
                .antMatchers(HttpMethod.DELETE, "/teams").hasRole("VISITOR").anyRequest().authenticated();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}