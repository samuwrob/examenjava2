package fr.campus.academy.backenddev.examen.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @OneToMany
    @JoinColumn(name = "team_name")
    private List<Player> players;

    @OneToOne(cascade = CascadeType.PERSIST)
    private Coach coach;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "tournament_team", joinColumns = { @JoinColumn(name = "team_id") }, inverseJoinColumns = {
            @JoinColumn(name = "tournament_id") })
    private List<Tournament> tournaments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public List<Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournament> tournaments) {
        this.tournaments = tournaments;
    }

}
