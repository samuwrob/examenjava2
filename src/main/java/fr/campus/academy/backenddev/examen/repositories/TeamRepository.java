package fr.campus.academy.backenddev.examen.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.campus.academy.backenddev.examen.models.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {

}
