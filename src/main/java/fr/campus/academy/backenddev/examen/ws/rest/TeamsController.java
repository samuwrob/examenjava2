package fr.campus.academy.backenddev.examen.ws.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.campus.academy.backenddev.examen.models.Team;
import fr.campus.academy.backenddev.examen.repositories.TeamRepository;
import fr.campus.academy.backenddev.examen.ws.rest.dto.TeamDTO;
import io.micrometer.core.ipc.http.HttpSender.Response;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "teams")
public class TeamsController {

    private final TeamRepository teamRepository;

    public TeamsController(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @GetMapping
    public ResponseEntity<List<TeamDTO>> getAllTeams() {
        return ResponseEntity.ok(this.teamRepository.findAll().stream()
                .map(team -> new TeamDTO(team.getId(), team.getName())).collect(Collectors.toList()));
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDTO> createTeams(@RequestBody TeamDTO teamDTO) {
        Team team = new Team();
        team.setName(teamDTO.getName());

        Team createdTeam = this.teamRepository.save(team);
        TeamDTO teamDTO1 = new TeamDTO(createdTeam.getId(), createdTeam.getName());
        return ResponseEntity.ok(teamDTO1);

    }

    @DeleteMapping(path = "{id}") //
    public void deleteTeam(@PathVariable Long id) {
        this.teamRepository.deleteById(id);
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDTO> updateTeams(@PathVariable Long id, @RequestBody TeamDTO teamDTO) {
        Team team;
        team = this.teamRepository.getOne(id);
        team.setName(teamDTO.getName());

        Team updatedTeam = this.teamRepository.save(team);
        return ResponseEntity.ok(new TeamDTO(updatedTeam.getId(), updatedTeam.getName()));
    }

}
