package fr.campus.academy.backenddev.examen.ws.rest.dto;

public class TeamDTO {
    private Long id;
    private String name;
    // private List<Player> players;
    // private Coach coach;
    // private List<Tournament> tournaments;

    public TeamDTO() {

    }

    public TeamDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
