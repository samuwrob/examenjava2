package fr.campus.academy.backenddev.examen.ws.rest.dto;

public class UserDTO {

    private String username;
    private String mail;

    public UserDTO() {
    }

    public UserDTO(String username, String mail) {
        this.username = username;
        this.mail = mail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
